using Gpseq;

void main () 
    var list_of_otstups = new Gee.ArrayList<int>();
    var lines = file_to_string("testself.vala").split("\n");
    int c = 1;
    list_of_otstups = from_code_to_otstups(lines);
    foreach(var a in list_of_otstups) {prin(c++,lines[c-2],":\t",a);}
    prin("debug");
    
    sas(list_of_otstups, lines);
    var builder = new StringBuilder();
    prin("debug");
    foreach(var a in lines) 
        builder.append(a);
        builder.append("\n");
    
    prin(builder.str);
    FileUtils.set_contents("output.vala",builder.str);




Gee.ArrayList<int>  from_code_to_otstups(string[] lines)
    return Seq.of_array<string>(lines)
        .map<int>(g => g.chug().has_prefix(".")? space_counter(g.to_utf8())/4-1: space_counter(g.to_utf8())/4)
        .collect_ordered(Collectors.to_list())
        .value as Gee.ArrayList<int>;


void sas (Gee.ArrayList<int> array,  string[] lines)
    for (int i = 0; i < array.size-1; i++)
        prin(i," ",array[i]," ",array.size);
        
        if (array[i] > array[i+1]) lines[i] = lines[i] + add_brackets_on_diffetence(array[i], array[i+1]);//"\n" + string.nfill(array[i+1] == -1?0:array[i+1],'\t') + "}";
        else if (array[i] < array[i+1]) lines[i] = lines[i] + "{";


string file_to_string(string filename)
    string s = "";
    FileUtils.get_contents(filename, out s);
    return s;


string add_brackets_on_diffetence(int a, int b)
    var build = new StringBuilder("\n"); 
    for (int i = a - b; i > 0; i--)
        build.append(string.nfill(i!=0?i-1:i,'\t'));
        build.append("}\n");
    return build.str;


int space_counter(char[] arr)
    var counter = 0;
    foreach (unowned char a in arr)
        if (a == ' ') ++counter;
        else break;
    
    return counter;


[Print]
void prin(string s)
    print(s + "\n");

